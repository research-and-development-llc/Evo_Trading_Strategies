var searchData=
[
  ['rainbow_5foption_5fprice_2em',['rainbow_option_price.m',['../rainbow__option__price_8m.html',1,'']]],
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['results',['RESULTS',['../namespaceresults__processing.html#ac6c96a2e79f15cf92f8e20f5798cf2ca',1,'results_processing']]],
  ['results_5fprocessing',['results_processing',['../namespaceresults__processing.html',1,'']]],
  ['results_5fprocessing_2epy',['results_processing.py',['../results__processing_8py.html',1,'']]],
  ['rfr_3abasis_3ahttps_3aassetprices_3avols_3adivs_3astrike_3acorrs_3aoptspec_3amaxmin_3aprices_3a',['rfr:basis:https:assetPrices:vols:divs:strike:corrs:optSpec:maxmin:prices:',['../rainbow__option__price_8m.html#a11aa22d2ff9d6b11aa7922b863aedf57',1,'rainbow_option_price.m']]],
  ['rfrs',['rfrs',['../namespacedata__formatting.html#ad15198347d406ec630f0711542b8f99a',1,'data_formatting']]],
  ['run',['run',['../classtrader__sim__multi_1_1_trader_sim.html#a5e26a9e926b2e0e6b2a8cd5df9eae5cf',1,'trader_sim_multi.TraderSim.run()'],['../classtrader__sim__single_1_1_trader_sim.html#a198191bcc5a90eb7fa5e5a7361d987e4',1,'trader_sim_single.TraderSim.run()']]],
  ['run_5fall_5fex',['run_all_ex',['../namespacerun__all__ex.html',1,'']]],
  ['run_5fall_5fex_2epy',['run_all_ex.py',['../run__all__ex_8py.html',1,'']]],
  ['runs',['RUNS',['../namespacetrader__sim__multi.html#a77d4064912cc4f700968de561c85111c',1,'trader_sim_multi.RUNS()'],['../namespacetrader__sim__single.html#ae0b5e5a516be03105ed70e5ce40a3449',1,'trader_sim_single.RUNS()']]]
];
