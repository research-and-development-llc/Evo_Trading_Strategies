var searchData=
[
  ['bestlog',['bestLog',['../namespacetrader__sim__multi.html#a798e40109067aa855471af1d2e2e99e2',1,'trader_sim_multi']]],
  ['bestprofit',['bestProfit',['../namespacetrader__sim__multi.html#ae51a97b647e980f0b8394f4dc5c9944e',1,'trader_sim_multi']]],
  ['bests',['bests',['../namespacetrader__sim__multi.html#a556e735d9d678bfe235d415f8a1c9502',1,'trader_sim_multi.bests()'],['../namespacetrader__sim__single.html#a6f71c8ff296934cfd82a6337d339b854',1,'trader_sim_single.bests()']]],
  ['buy_5fcall_5fhigh',['buy_call_high',['../classtrader__sim__multi_1_1_trader_sim.html#a0d65eaf6f69e0d588ca03187919c3bf8',1,'trader_sim_multi.TraderSim.buy_call_high()'],['../classtrader__sim__single_1_1_trader_sim.html#a07f763082293c612995724a6de914cee',1,'trader_sim_single.TraderSim.buy_call_high()']]],
  ['buy_5fcall_5flow',['buy_call_low',['../classtrader__sim__multi_1_1_trader_sim.html#afb26d482af85d58a83cde62247e86508',1,'trader_sim_multi.TraderSim.buy_call_low()'],['../classtrader__sim__single_1_1_trader_sim.html#a5c7d016e54172c647da3858c23c53fd5',1,'trader_sim_single.TraderSim.buy_call_low()']]],
  ['buy_5fput_5fhigh',['buy_put_high',['../classtrader__sim__multi_1_1_trader_sim.html#a4f923dc286b283ba61ca2e882f049ec9',1,'trader_sim_multi.TraderSim.buy_put_high()'],['../classtrader__sim__single_1_1_trader_sim.html#a380c400596a9bfb411f9ce76169ee6b0',1,'trader_sim_single.TraderSim.buy_put_high()']]],
  ['buy_5fput_5flow',['buy_put_low',['../classtrader__sim__multi_1_1_trader_sim.html#aa1c577c7c0da22b474be03cded68eb5b',1,'trader_sim_multi.TraderSim.buy_put_low()'],['../classtrader__sim__single_1_1_trader_sim.html#a5f81a5b83276f82e5cda2bb5465dbf41',1,'trader_sim_single.TraderSim.buy_put_low()']]],
  ['buy_5fu1',['buy_u1',['../classtrader__sim__multi_1_1_trader_sim.html#acd6e46eca0c60071edfde16cee415c4d',1,'trader_sim_multi.TraderSim.buy_u1()'],['../classtrader__sim__single_1_1_trader_sim.html#a8201c3f0d92744fdd94784fd0ce808c3',1,'trader_sim_single.TraderSim.buy_u1()']]],
  ['buy_5fu2',['buy_u2',['../classtrader__sim__multi_1_1_trader_sim.html#ab8ae5e7cfc28a130a2c7991750e414b9',1,'trader_sim_multi.TraderSim.buy_u2()'],['../classtrader__sim__single_1_1_trader_sim.html#a48f96edd7d940415ca6af29dfb8c8699',1,'trader_sim_single.TraderSim.buy_u2()']]]
];
