var namespaces =
[
    [ "data_2_trader", "namespacedata__2__trader.html", null ],
    [ "data_formatting", "namespacedata__formatting.html", null ],
    [ "data_processing", "namespacedata__processing.html", null ],
    [ "ex_runner", "namespaceex__runner.html", null ],
    [ "exoticOptionsGP", "namespaceexotic_options_g_p.html", null ],
    [ "generalization_plots", "namespacegeneralization__plots.html", null ],
    [ "process_data", "namespaceprocess__data.html", null ],
    [ "results_processing", "namespaceresults__processing.html", null ],
    [ "run_all_ex", "namespacerun__all__ex.html", null ],
    [ "trader_sim_multi", "namespacetrader__sim__multi.html", null ],
    [ "trader_sim_single", "namespacetrader__sim__single.html", null ]
];