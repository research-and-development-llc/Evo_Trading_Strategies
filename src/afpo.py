"""!
Implements Age Fitness Pareto Optimization (AFPO) and supporting functions.

@author Colin M. Van Oort
"""


from deap import algorithms, tools
from deap.gp import cxOnePoint
import matplotlib.pyplot as plt


def afpo(population,
         tool_box,
         crossover_rate,
         mutation_rate,
         generation_count,
         births_per_generation=0,
         stats=None,
         hall_of_fame=None,
         verbose=__debug__,
         demo=False):
    """!
    Implements Age Fitness Pareto Optimization.

    @param population (list)
    @param tool_box (deap.base.Toolbox) Holds evolutionary operators.
    @param crossover_rate (float)
    @param mutation_rate (float)
    @param generation_count (int)
    @param births_per_generation (int)
    @param stats (deap.tools.Statistics) Logs evolution dynamics.
    @param hall_of_fame (deap.tools.HallOfFame) Holds best individuals.
    @param verbose Terminal output control.

    @return (list) Evolved population, evolution logbook.
    """
    assert births_per_generation >= 0

    # Plotting structures used for the demo, should be removed in the future.
    if demo:
        fig, ax = plt.subplots()
        ax.set_title('Profit of the best strategy found during evolution')
        ax.set_xlabel('Generation')
        ax.set_ylabel('Cumulative Profit Ratio')
        plt.ion()

        # Maximize the window size
        mng = plt.get_current_fig_manager()
        mng.window.showMaximized()

        plt.show()

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_individuals = [ind for ind in population if not ind.fitness.valid]
    fitnesses = tool_box.map(tool_box.evaluate, invalid_individuals)
    for ind, fit in zip(invalid_individuals, fitnesses):
        ind.fitness.values = fit

    if hall_of_fame is not None:
        hall_of_fame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_individuals), **record)

    if verbose:
        print(logbook.stream)

    # The generational loop
    for gen in range(1, generation_count + 1):
        offspring = tool_box.select(population,
                                    len(population) - births_per_generation)

        offspring = algorithms.varAnd(offspring, tool_box,
                                      crossover_rate, mutation_rate)

        offspring += [tool_box.individual()
                      for _ in range(births_per_generation)]

        invalid_individuals = [ind for ind in offspring
                               if not ind.fitness.valid]

        fitnesses = tool_box.map(tool_box.evaluate, invalid_individuals)

        for ind, fit in zip(invalid_individuals, fitnesses):
            ind.fitness.values = fit

        if hall_of_fame is not None:
            hall_of_fame.update(offspring)

        population[:] = offspring

        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_individuals), **record)

        if verbose:
            print(logbook.stream)

        for individual in population:
            individual.age += 1

        if demo:
            ax.plot(range(gen + 1),
                    logbook.chapters['fitness'].select('max')[:],
                    'k-',
                    linewidth=4)

            plt.draw()
            plt.pause(0.05)

    return population, logbook


def tournament_selection(individuals, tourncount, tournsize):
    """!
    @param individuals (list) Potential tournament contestants.

    @param tourncount (int)

    @param tournsize (int)

    @return (list) Winners of each tournament.
    """
    selected = []

    for i in range(tourncount):
        contestants = tools.selRandom(individuals, tournsize)
        selected.append(tools.sortLogNondominated(contestants, 1, first_front_only=True)[0])

    return selected


def one_point_crossover(ind1, ind2):
    """!
    Randomly select a subtree in each individual and then swap those subtrees.

    @param ind1 (deap.gp.PrimitiveTree)

    @param ind2 (deap.gp.PrimitiveTree)

    @return (tuple) Two individuals resulting from crossover.
    """
    ind1, ind2 = cxOnePoint(ind1, ind2)

    ind1.age = max(ind1.age, ind2.age)
    ind2.age = max(ind1.age, ind2.age)

    return ind1, ind2
