"""!
A library used for calculating option prices.

@author David Dewhurst
"""


from __future__ import print_function
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


def moving_average(array, order):
    """!
    Calculates the moving average of an array to the specified order.

    @param array (1-d array): Data to calculate moving average from.
    @param order (int): The order to which the moving average is calculated.

    @return float: The moving average.
    """
    ret = np.cumsum(array, dtype=float)
    ret[order:] = ret[order:] - ret[:-order]
    return ret[order - 1:] / order


def historical_volatility(array, num_days=252):
    """!
    Calculates the historical volatility of a security.

    @param array (1-d array): An array of security prices.
    @param num_days=256 (int): The number of days over which to calculate the
        historical volatility.

    @return (float): The historic volatility of the security.
    """

    log_daily_returns = [np.log(array[i] / array[i - 1])
            for i in range(1, len(array))]
    return np.sqrt(num_days * np.var(log_daily_returns))


class time_series_viz(object):
    """!
    A time series visualization class that provides limited methods by which
    to visualize multiple time series of similar time scale on the same axis.
    """

    def __init__(self, arrays):
        self.ts = arrays

    def standalone_display(self, labels=None, save=False, savename=None):
        """!
        Displays visualization in a standalone manner without assuming
        other axes are present.

        @param labels Labels for the plot, optional.
        @param save (bool): False displays plot true saves it, optional.
        @param savename (str): Location to save the plot, optional.
        """
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        try:
            [ax.plot(range(self.ts.shape[0]), self.ts[i, :], 'k-') for i in range(1, self.ts.shape[-1])]

        except IndexError:
            ax.plot(range(self.ts.shape[0]), self.ts, 'k-')
            ax.set_xticks([])

        if labels is not None:
            try:
                ax.set_xlabel(labels[0])
                ax.set_ylabel(labels[1])

            except:
                raise(IOError, 'labels specified incorrectly')

        if save is False:
            plt.show()
        elif save is True:
            assert(type(savename) is str)
            plt.savefig(savename)

    def append_to_plot(self, axes, labels=None):
        """!
        Appends the visualization to an already-created figure.

        @param axes (matplotlib.pyplot axes): A set of axes for the plot.
        @param labels Labels for the plot, optional.
        """
        try:
            [axes.plot(range(self.ts.shape[0]), self.ts[i, :], 'k-') for i in range(1, self.ts.shape[-1])]

        except IndexError:
            axes.plot(range(self.ts.shape[0]), self.ts, 'k-')

            if labels is not None:
                try:
                    axes.set_xlabel(labels[0])
                    axes.set_ylabel(labels[1])
                except:
                    raise(IOError, 'labels specified incorrectly')


def high_low_strike_prices(array, prop=0.1):
    """!
    Calculates a "high" and "low" strike price for an option on an asset for
    heuristic use in genetic programming.

    @param array (np array): The security price data.
    @param prop=0.1 (float): Proportion up or down for the high and low price.

    @return high (np array): Array of high strike prices.
    @return low (np array): Array of low strike prices.
    """
    high = [(1. + prop) * price for price in array]
    low = [(1. - prop) * price for price in array]

    return np.asarray(high), np.asarray(low)


def jensen_shannon_divergence(p, q):
    """!
    @param p (np array): The first probability distribution.
    @param q (np array): The second probability distribution.

    @return The Jensen-Shannon divergence of the two distributions.
    """
    p = np.asarray(p)
    q = np.asarray(q)
    m = 0.5 * (p + q)

    return 0.5 * (stats.entropy(p, m) + stats.entropy(q, m))
