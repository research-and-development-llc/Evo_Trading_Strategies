"""!
Implements a trading simulator class which allows for the evaluation of trading
strategies in a single action per time step scheme. Strategies are evolved
using AFPO.

See trader_sim_multi.py for detailed function comments.

@author John H. Ring IV
@author Colin M. Van Oort
"""

from __future__ import print_function

from deap import base
from deap import creator
from deap import gp
from deap import tools
from functools import partial
from scoop import futures
import cPickle as pickle
import matplotlib.pyplot as plt
import numpy as np
import operator
import sys

import exoticOptionsGP


class TraderSim(object):
    def __init__(self, cash, fee, ch, cl, ph, pl):
        self.load_data(ch, cl, ph, pl)
        self.start_cash = cash
        self.max_steps = len(self.CH) - 1
        self.steps = 0
        self.cash = cash
        self.fee = fee
        self.holdings = [0, 0, 0, 0, 0, 0]
        self.values = np.zeros(self.max_steps)
        self.values[0] = cash
        self.algo = None

    def _reset(self):
        self.steps = 0
        self.cash = self.start_cash
        self.holdings = [0, 0, 0, 0, 0, 0]
        self.values = np.zeros(self.max_steps+1)
        self.values[0] = self.cash

    def get_call_high(self):
        return self.CH[self.steps]

    def get_call_low(self):
        return self.CL[self.steps]

    def get_put_high(self):
        return self.PH[self.steps]

    def get_put_low(self):
        return self.PL[self.steps]

    def get_value(self):
        return self.cash + \
            self.holdings[0] * self.get_call_high() + \
            self.holdings[1] * self.get_call_low() + \
            self.holdings[2] * self.get_put_high() + \
            self.holdings[3] * self.get_put_low() + \
            self.holdings[4] * self.U1[self.steps] + \
            self.holdings[5] * self.U2[self.steps]

    def buy_call_high(self):
        if self.steps < self.max_steps:
            price = self.get_call_high() + self.fee
            if self.cash >= price:
                self.holdings[0] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def buy_call_low(self):
        if self.steps < self.max_steps:
            price = self.get_call_low() + self.fee
            if self.cash >= price:
                self.holdings[1] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def buy_put_high(self):
        if self.steps < self.max_steps:
            price = self.get_put_low() + self.fee
            if self.cash >= price:
                self.holdings[2] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def buy_put_low(self):
        if self.steps < self.max_steps:
            price = self.get_put_low() + self.fee
            if self.cash >= price:
                self.holdings[3] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def buy_u1(self):
        if self.steps < self.max_steps:
            price = self.U1[self.steps] + self.fee
            if self.cash >= price:
                self.holdings[4] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def buy_u2(self):
        if self.steps < self.max_steps:
            price = self.U2[self.steps] + self.fee
            if self.cash >= price:
                self.holdings[5] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def sell_call_high(self):
        if self.steps < self.max_steps:
            if self.holdings[0] > 0:
                self.cash += self.get_call_high() - self.fee
                self.holdings[0] -= 1
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def sell_call_low(self):
        if self.steps < self.max_steps:
            if self.holdings[1] > 0:
                self.cash += self.get_call_low() - self.fee
                self.holdings[1] -= 1
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def sell_put_high(self):
        if self.steps < self.max_steps:
            if self.holdings[2] > 0:
                self.cash += self.get_put_high() - self.fee
                self.holdings[2] -= 1
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def sell_put_low(self):
        if self.steps < self.max_steps:
            if self.holdings[3] > 0:
                self.cash += self.get_put_high() - self.fee
                self.holdings[3] -= 1
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def sell_u1(self):
        if self.steps < self.max_steps:
            if self.holdings[4] > 0:
                self.cash += self.U1[self.steps] - self.fee
                self.holdings[4] -= 1
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def sell_u2(self):
        if self.steps < self.max_steps:
            if self.holdings[5] > 0:
                self.cash += self.U1[self.steps] - self.fee
                self.holdings[5] -= 1
                self.values[self.steps] = self.get_value()
            self.steps += 1

    def do_nothing(self):
        if self.steps < self.max_steps:
            self.steps += 1
            self.values[self.steps] = self.get_value()

    def up_total(self):
        return self.values[self.steps] > self.values[0]

    def up_day(self):
        ret = True
        if self.steps >= 1:
            ret = self.values[self.steps] > self.values[self.steps - 1]
        return ret

    def up_week(self):
        ret = True
        if self.steps >= 5:
            ret = self.values[self.steps] > self.values[self.steps - 5]
        return ret

    def u1_up_total(self):
        return self.U1[self.steps] > self.U1[0]

    def u1_up_day(self):
        ret = True
        if self.steps >= 1:
            ret = self.U1[self.steps] > self.U1[self.steps - 1]
        return ret

    def u1_up_week(self):
        ret = True
        if self.steps >= 5:
            ret = self.U1[self.steps] > self.U1[self.steps - 5]
        return ret

    def u2_up_total(self):
        return self.U2[self.steps] > self.U2[0]

    def u2_up_day(self):
        ret = True
        if self.steps >= 1:
            ret = self.U2[self.steps] > self.U2[self.steps - 1]
        return ret

    def u2_up_week(self):
        ret = True
        if self.steps >= 5:
            ret = self.U2[self.steps] > self.U2[self.steps - 5]
        return ret

    def holding_call_high(self):
        return self.holdings[0] > 0

    def holding_call_low(self):
        return self.holdings[1] > 0

    def holding_put_high(self):
        return self.holdings[2] > 0

    def holding_put_low(self):
        return self.holdings[3] > 0

    def holding_u1(self):
        return self.holdings[4] > 0

    def holding_u2(self):
        return self.holdings[5] > 0

    def if_up_total(self, out1, out2):
        return partial(if_then_else, self.up_total, out1, out2)

    def if_up_day(self, out1, out2):
        return partial(if_then_else, self.up_day, out1, out2)

    def if_up_week(self, out1, out2):
        return partial(if_then_else, self.up_week, out1, out2)

    def if_u1_up_total(self, out1, out2):
        return partial(if_then_else, self.u1_up_total, out1, out2)

    def if_u1_up_day(self, out1, out2):
        return partial(if_then_else, self.u1_up_day, out1, out2)

    def if_u1_up_week(self, out1, out2):
        return partial(if_then_else, self.u1_up_week, out1, out2)

    def if_u2_up_total(self, out1, out2):
        return partial(if_then_else, self.u2_up_total, out1, out2)

    def if_u2_up_day(self, out1, out2):
        return partial(if_then_else, self.u2_up_day, out1, out2)

    def if_u2_up_week(self, out1, out2):
        return partial(if_then_else, self.u2_up_week, out1, out2)

    def if_holding_call_high(self, out1, out2):
        return partial(if_then_else, self.holding_call_high, out1, out2)

    def if_holding_call_low(self, out1, out2):
        return partial(if_then_else, self.holding_call_low, out1, out2)

    def if_holding_put_high(self, out1, out2):
        return partial(if_then_else, self.holding_put_high, out1, out2)

    def if_holding_put_low(self, out1, out2):
        return partial(if_then_else, self.holding_put_low, out1, out2)

    def if_holding_u1(self, out1, out2):
        return partial(if_then_else, self.holding_u1, out1, out2)

    def if_holding_u2(self, out1, out2):
        return partial(if_then_else, self.holding_u2, out1, out2)

    def run(self, algo):
        self._reset()
        while self.steps < self.max_steps:
            algo()

    def load_data(self, chStr, clStr, phStr, plStr):
        chFile = open(chStr, "r")
        clFile = open(clStr, "r")
        phFile = open(phStr, "r")
        plFile = open(plStr, "r")

        ch = pickle.load(chFile)
        cl = pickle.load(clFile)
        ph = pickle.load(phFile)
        pl = pickle.load(plFile)

        self.CH = [float(el[14]) for el in ch[1:]]
        self.CL = [float(el[14]) for el in cl[1:]]
        self.PH = [float(el[14]) for el in ph[1:]]
        self.PL = [float(el[14]) for el in pl[1:]]
        self.U1 = [float(el[4]) for el in pl[1:]]
        self.U2 = [float(el[5]) for el in pl[1:]]

        chFile.close()
        clFile.close()
        phFile.close()
        plFile.close()


def progn(*args):
    for arg in args:
        arg()


def prog2(out1, out2):
    return partial(progn, out1)


def prog3(out1, out2, out3):
    return partial(progn, out1, out2, out3)


def prog4(out1, out2, out3, out4):
    return partial(progn, out1, out2, out3, out4)


def if_then_else(condition, out1, out2):
    out1() if condition() else out2()


def evalTrader(individual):
    # Transform the tree expression into functional Python code
    algo = gp.compile(individual, pset)

    trader.run(algo)

    profit = trader.values[trader.steps] - trader.values[0]

    return (profit, individual.age,)


def testTrader(individual):
    # Transform the tree expression into functional Python code
    algo = gp.compile(individual, pset)

    trader.run(algo)

    profit = trader.values[trader.steps] - trader.values[0]

    return profit


CH = sys.argv[1]
CL = sys.argv[2]
PH = sys.argv[3]
PL = sys.argv[4]
ex = sys.argv[5]

# Initialize a trader class with market data and starting resources
trader = TraderSim(10000, 0, CH, CL, PH, PL)

# Assemble the primitive set used in the GP
pset = gp.PrimitiveSet("MAIN", 0)
pset.addPrimitive(trader.if_up_total, 2)
pset.addPrimitive(trader.if_up_day, 2)
pset.addPrimitive(trader.if_up_week, 2)
pset.addPrimitive(prog2, 2)
pset.addPrimitive(prog3, 3)
pset.addPrimitive(prog4, 4)
pset.addPrimitive(trader.if_holding_call_high, 2)
pset.addPrimitive(trader.if_holding_call_low, 2)
pset.addPrimitive(trader.if_holding_put_high, 2)
pset.addPrimitive(trader.if_holding_put_low, 2)
pset.addPrimitive(trader.if_u1_up_day, 2)
pset.addPrimitive(trader.if_u1_up_week, 2)
pset.addPrimitive(trader.if_u1_up_total, 2)
pset.addPrimitive(trader.if_u2_up_day, 2)
pset.addPrimitive(trader.if_u2_up_week, 2)
pset.addPrimitive(trader.if_u2_up_total, 2)
pset.addPrimitive(trader.if_holding_u1, 2)
pset.addPrimitive(trader.if_holding_u2, 2)
pset.addTerminal(trader.buy_call_high)
pset.addTerminal(trader.buy_call_low)
pset.addTerminal(trader.buy_put_low)
pset.addTerminal(trader.buy_put_high)
pset.addTerminal(trader.do_nothing)
if int(ex) == 2:
    pset.addTerminal(trader.buy_u1)
    pset.addTerminal(trader.buy_u2)
    pset.addTerminal(trader.sell_u1)
    pset.addTerminal(trader.sell_u2)


# Begin assembly of toolbox used in the GP
toolbox = base.Toolbox()

# Structure initializers
creator.create("FitnessMulti", base.Fitness, weights=(1.0, -1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMulti, pset=pset, age=1)

# Expression tree generator
toolbox.register("expr_init", gp.genHalfAndHalf, pset=pset, min_=1, max_=2)

toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr_init)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("evaluate", evalTrader)
toolbox.register("select", exoticOptionsGP.selTournamentAFPO, tournsize=3)
toolbox.register("mate", exoticOptionsGP.cxOnePointAFPO)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=2)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)
toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))
toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))

# Multiprocessing using scoop
toolbox.register("map", futures.map)


def evolveTradingStrategies():
    pop = toolbox.population(n=50)
    hof = tools.HallOfFame(5)

    stats_fit = tools.Statistics(lambda ind: ind.fitness.values[0])
    stats_age = tools.Statistics(lambda ind: ind.fitness.values[1])
    stats_size = tools.Statistics(key=len)

    mstats = tools.MultiStatistics(fitness=stats_fit, age=stats_age, size=stats_size)

    mstats.register("min", np.min)
    mstats.register("max", np.max)
    mstats.register("avg", np.mean)
    mstats.register("std", np.std)

    pop, logbook = exoticOptionsGP.afpo(pop, toolbox, 0.5, 0.2, 50, 5, mstats, halloffame=hof, verbose=False)

    return pop, logbook, hof


if __name__ == '__main__':
    EXPERIMENT = 1
    RUNS = 100

    if len(sys.argv) > 6:
        EXPERIMENT = -1
        ind = gp.PrimitiveTree.from_string(str(sys.argv[6]), pset)
        profit = testTrader(ind)
        f1 = open('result.txt', 'w')
        f1.write(str(profit))
        f1.close()

    if EXPERIMENT is None:
        # testing block
        pop, logbook, hof = evolveTradingStrategies()
        print("\n\n", hof[0], logbook.chapters['fitness'].select('max')[-1], logbook.chapters['fitness'].select('avg')[-1], "\n\n")

    elif EXPERIMENT == 1:
        """ Here, we'll run a lot of runs on the same underlyings and see
        what type of individuals we get out
        """
        bests = []
        max_fitness = []
        avg_size = []

        for _ in range(RUNS):
            pop, logbook, hof = evolveTradingStrategies()
            bests.append(hof[0].__str__())
            max_fitness.append(logbook.chapters['fitness'].select('max'))
            avg_size.append(logbook.chapters['size'].select('avg'))

        max_fitness = np.asarray(max_fitness)
        avg_size = np.asarray(avg_size)

        avgMaxFit = np.mean(max_fitness, axis=0)
        devMaxFit = np.std(max_fitness, axis=0)

        avgAvgSize = np.mean(avg_size, axis=0)
        devAvgSize = np.std(avg_size, axis=0)

        # plot the results
        gen = logbook.select('gen')  # same number of generations for all runs
        fig, ax1 = plt.subplots()
        plt.hold(True)
        for i in range(RUNS):
            ax1.plot(gen, max_fitness[i], 'k-', alpha=0.5)

        ax1.plot(gen, avgMaxFit, 'w-', linewidth=10)
        avg_prof, = ax1.plot(gen, avgMaxFit, 'k-', linewidth=5, label='Average of maximum profits')

        ax1.set_xlabel('Generation')
        ax1.set_ylabel('Maximum profit', color='k')
        plt.xlim(0, 50)
        plt.legend(handles=[avg_prof])
        plt.savefig('ex{}_single_100_runs.png'.format(ex))

        with open('ex{}_single_best_indivs_100_runs.pkl'.format(ex), 'wb') as f:
            pickle.dump([bests, logbook], f)
