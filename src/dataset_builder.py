#!/usr/bin/env python


from pathlib import Path
import pandas as pd
import random


def build_dataset(stocks=[]):
    if not stocks:
        stocks = sorted([x.stem for x in Path('../data/clean').glob('*.csv')])
        stocks = random.sample(stocks, random.randrange(3, len(stocks) // 10))

    frames = []

    for stock in stocks:
        p = Path('../data/clean/{}.csv'.format(stock))

        if p.is_file():
            df = pd.read_csv(str(p), index_col=0)
            df.rename(columns={'close_price': stock}, inplace=True)
            frames.append(df)

    return pd.concat(frames, axis=1, join='inner')


if __name__ == '__main__':
    data = build_dataset()

    print(data)

    X = data.values.T

    print(X.shape)
