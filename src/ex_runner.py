"""!
A simple script to run the trading simulation on many combinations
of dates and securities and then evaluate them on all possible data sets.

This script takes two command line arguments ex and ver.
When ex=1 the simulation runs without the ability to trade the underlying
securities for ex=2 this restrict is lifted. ver may be set to single or multi
when ver is single the algorithm may only preform one action per day. When ver
is multi this restriction is lifted.

@author John H. Ring IV
@author David Dewhurst
"""


from itertools import product
import os
import pickle
import subprocess
import sys


if __name__ == "__main__":
    ex = str(sys.argv[1])  # experiment 1 or 2
    ver = str(sys.argv[2])  # single or multi

    DATES = [['19980105', '19980506', '19990105'],
             ['19980506', '19981106', '19990506'],
             ['19990105', '19990506', '20000105'],
             ['19990506', '19991106', '20000506'],
             ['20000105', '20000506', '20010105'],
             ['20000506', '20001106', '20010506'],
             ['20010105', '20010506', '20020105'],
             ['20010506', '20011106', '20020506'],
             ['20020105', '20020506', '20030105'],
             ['20020506', '20021106', '20030506'],
             ['20030105', '20030506', '20040105'],
             ['20030506', '20031106', '20040506']]

    SECURITIES = [['aapl', 't'],
                  ['ibm', 'xom'],
                  ['msft', 'x']]

    d = '../data/processed/'

    for date, sec in product(DATES, SECURITIES):
        n = "{}-{}-{}{}-{}.pkl".format(*date, *sec)
        path = '../results/{}-{}-{}{}-{}'.format(*date, *sec)

        if not os.path.exists(path):
            os.makedirs(path)

        subprocess.call('python -m scoop trader_sim_{}.py {}ch_{} {}cl_{} {}ph_{} {}pl_{} {}'.format(ver, d, n, d, n, d, n, d, n, ex), shell=True)
        subprocess.call('mv ex{}_{}_100_runs.png ex{}_{}_best_indivs_100_runs.pkl {}'.format(ex, ver, ex, ver, path), shell=True)

        data = {}
        fName = 'ex{}_{}_best_indivs_100_runs.pkl'.format(ex, ver)
        solLoc = "{}-{}-{}{}-{}".format(*date, *sec)

        with open('../results/' + solLoc + "/" + fName, "rb") as solFile:
            sol = str(pickle.load(solFile)[0][-1])

        for date2, sec2 in product(DATES, SECURITIES):
            path = '{}-{}-{}{}-{}'.format(*date2, *sec2)
            n = "{}-{}-{}{}-{}.pkl".format(*date2, *sec2)

            subprocess.call('python trader_sim_{}.py {}ch_{} {}cl_{} {}ph_{} {}pl_{} {} "{}"'.format(ver, d, n, d, n, d, n, d, n, ex, sol), shell=True)

            with open('result.txt', 'r') as f:
                a = float(f.readline())

            try:
                data[solLoc].append([path, a])

            except KeyError:
                data[solLoc] = [[path, a, sol]]

        with open('../results/ex{}_{}generalization_result.pkl'.format(ex, ver), 'wb') as result_file:
            pickle.dump(data, result_file)
