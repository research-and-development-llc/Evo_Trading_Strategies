#!usr/bin/env python


from pathlib import Path
from datetime import datetime


def main():
    data_path = Path('../raw')
    raw_files = data_path.glob('*.csv')

    out_path = Path('../clean')
    out_path.mkdir(parents=True, exist_ok=True)

    for f in raw_files:
        new_name = f.name[6:]

        with f.open() as in_file, (out_path / new_name).open(mode='w') as out_file:
            out_file.write('date,close_price\n')
            for line in in_file:
                elements = line.strip().split(',')
                date = datetime.strptime(elements[0], '%Y%m%d').date()
                close_price = float(elements[5])
                out_file.write('{},{}\n'.format(date, close_price))


if __name__ == '__main__':
    main()
